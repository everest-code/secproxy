package main

import (
	"gitlab.com/everest-code/golib/log"
	"gitlab.com/everest-code/secproxy/db"
	"gitlab.com/everest-code/secproxy/handlers"
	"net/http"
	"strings"
)

type Server struct {
	apiPrefix   string
	proxy       *handlers.Proxy
	apiInternal *handlers.InternalAPI
}

func newServer(config *Config) *Server {
	database := db.GetDatabase(config.DB, config.Proxy.CacheTTL, logger)

	proxy, err := handlers.NewProxy(config.Proxy.Domain, logger, database)
	if err != nil {
		log.Panic(err.Error())
	}

	if config.Headers != nil {
		proxy.Headers = config.Headers
	}

	if config.Proxy.AllowedMethods != nil {
		proxy.AllowedMethods = config.Proxy.AllowedMethods
	}

	api := handlers.NewInternalApi(logger, database, config.Api.Key)

	return &Server{
		apiPrefix:   config.Api.Prefix,
		proxy:       proxy,
		apiInternal: api,
	}
}

func (server *Server) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	if strings.HasPrefix(req.URL.Path, server.apiPrefix) {
		server.apiInternal.CreateRouter(server.apiPrefix).ServeHTTP(res, req)
	} else {
		server.apiInternal.GlobalAuthMiddleware(server.proxy.ServeHTTP)(res, req)
	}
}
