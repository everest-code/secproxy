package db

import (
	"errors"
	"fmt"
	"github.com/go-redis/redis/v8"
	"gitlab.com/everest-code/secproxy/utils"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type LogResult struct {
	Route    string    `json:"route"`
	IssuedAt time.Time `json:"issuedAt"`
}

var BadCredentials = errors.New("domain or password are incorrect")

func (d *Database) CreateApiKey(domain string) (string, error) {
	pwd := utils.CreateRandomPassword()
	hashPwd, err := utils.GeneratePasswd(pwd)
	if err != nil {
		return "", err
	}

	err = d.db.Set(ctx, fmt.Sprintf("apiKey:%s", domain), hashPwd, -1).Err()
	if err != nil {
		return "", err
	}

	return pwd, nil
}

func (d *Database) KeyExists(domain string) (bool, error) {
	res, err := d.db.Exists(ctx, fmt.Sprintf("apiKey:%s", domain)).Result()
	if err != nil {
		return false, err
	}

	return res > 0, nil
}

func (d *Database) CheckApiKey(domain, passwd string) error {
	hashPwd, err := d.db.Get(ctx, fmt.Sprintf("apiKey:%s", domain)).Result()
	if err != nil {
		if err == redis.Nil {
			return BadCredentials
		}

		return err
	}

	ok, err := utils.VerifyPassword(hashPwd, passwd)
	if err != nil {
		return err
	}

	if !ok {
		return BadCredentials
	}

	return nil
}

func (d *Database) AddRequest(domain string, url *url.URL) {
	date := time.Now().UnixNano()
	template := utils.FormatURL(url)

	d.db.LPush(ctx, fmt.Sprintf("requests:apiKey:%s", domain), fmt.Sprintf("%d:%s", date, template))
	d.db.Incr(ctx, fmt.Sprintf("requests:%s", template))
}

func (d *Database) ComposeKeyLog(domain string) ([]LogResult, error) {
	entries, err := d.db.LRange(ctx, fmt.Sprintf("requests:apiKey:%s", domain), 0, -1).Result()
	if err != nil {
		return nil, err
	}

	res := make([]LogResult, 0)
	for _, entry := range entries {
		data := strings.SplitN(entry, ":", 2)
		dateInt, _ := strconv.ParseInt(data[0], 10, 64)
		date := time.Unix(0, dateInt)
		res = append(res, LogResult{
			Route:    data[1],
			IssuedAt: date,
		})
	}

	return res, nil
}
