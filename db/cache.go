package db

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-redis/redis/v8"
	"gitlab.com/everest-code/secproxy/utils"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

var (
	ctx        = context.Background()
	NoCacheErr = errors.New("no page found in cache")
)

func (d *Database) CreatePageCache(url *url.URL, header http.Header, body []byte, method string, status int) error {
	if d.cacheTtl == 0 {
		return nil
	}

	d.logger.Debug("Creating cache for URL %s", url.String())

	headerBody, err := json.Marshal(header)
	if err != nil {
		return err
	}

	ttl := time.Duration(d.cacheTtl) * time.Second
	d.db.Set(ctx, utils.FormatURLPrefix(fmt.Sprintf("cache:%s:headers", method), url), string(headerBody), ttl)
	d.db.Set(ctx, utils.FormatURLPrefix(fmt.Sprintf("cache:%s:body", method), url), string(body), ttl)
	d.db.Set(ctx, utils.FormatURLPrefix(fmt.Sprintf("cache:%s:status", method), url), fmt.Sprintf("%d", status), ttl)

	return nil
}

func (d *Database) GetPageCache(method string, url *url.URL) (http.Header, []byte, int, error) {
	body, err := d.db.Get(ctx, utils.FormatURLPrefix(fmt.Sprintf("cache:%s:headers", method), url)).Result()
	if err == redis.Nil {
		return nil, nil, -1, NoCacheErr
	} else if err != nil {
		return nil, nil, -1, err
	}

	headers := make(http.Header)
	err = json.Unmarshal([]byte(body), &headers)
	if err != nil {
		return nil, nil, -1, err
	}

	body, err = d.db.Get(ctx, utils.FormatURLPrefix(fmt.Sprintf("cache:%s:status", method), url)).Result()
	if err == redis.Nil {
		return nil, nil, -1, NoCacheErr
	} else if err != nil {
		return nil, nil, -1, err
	}

	status, err := strconv.Atoi(body)
	if err != nil {
		return nil, nil, -1, err
	}

	body, err = d.db.Get(ctx, utils.FormatURLPrefix(fmt.Sprintf("cache:%s:body", method), url)).Result()
	if err == redis.Nil {
		return nil, nil, -1, NoCacheErr
	} else if err != nil {
		return nil, nil, -1, err
	}

	return headers, []byte(body), status, nil
}
