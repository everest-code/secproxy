package db

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/everest-code/golib/log"
)

type Database struct {
	db       *redis.Client
	cacheTtl int
	logger   *log.Logger
}

func GetDatabase(opts *redis.Options, cacheTtl int, logger *log.Logger) *Database {
	return &Database{
		db:       redis.NewClient(opts),
		cacheTtl: cacheTtl,
		logger:   logger,
	}
}
