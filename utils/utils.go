package utils

import (
	"fmt"
	"net/url"
)

func FormatURL(u *url.URL) string {
	return u.String()
}

func FormatURLPrefix(prefix string, u *url.URL) string {
	return fmt.Sprintf("%s:%s", prefix, FormatURL(u))
}

func InArray(haystack []string, value string) bool {
	for _, v := range haystack {
		if v == value {
			return true
		}
	}
	return false
}
