package utils

import (
	"github.com/tredoe/osutil/v2/userutil/crypt"
	"github.com/tredoe/osutil/v2/userutil/crypt/common"
	salt "github.com/tredoe/osutil/v2/userutil/crypt/md5_crypt"
)

func createCrypter() crypt.Crypter {
	return crypt.New(crypt.MD5)
}

func createSalt() common.Salt {
	return salt.GetSalt()
}

func generateSalt() []byte {
	salt := createSalt()
	return salt.Generate(salt.SaltLenMax)
}

func GeneratePasswd(password string) (string, error) {
	return createCrypter().Generate([]byte(password), generateSalt())
}

func VerifyPassword(passwd, password string) (bool, error) {
	err := createCrypter().Verify(passwd, []byte(password))
	if err != nil && err != crypt.ErrKeyMismatch {
		return false, err
	}

	return err == nil, nil
}
