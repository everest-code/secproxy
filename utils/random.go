package utils

import (
	"math/rand"
	"time"
)

const haystack = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#$%&{}[]|"

func CreateRandomPassword() string {
	rand.NewSource(time.Now().UnixNano())
	pwd := ""

	for i := 0; i < 32; i++ {
		pwd += string(haystack[rand.Intn(len(haystack))])
	}

	return pwd
}
