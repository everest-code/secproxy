package handlers

import (
	"github.com/docker/go-units"
	"gitlab.com/everest-code/golib/api"
	"gitlab.com/everest-code/golib/log"
	"gitlab.com/everest-code/secproxy/db"
	"gitlab.com/everest-code/secproxy/utils"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

type Proxy struct {
	Domain         *url.URL
	Headers        map[string]string
	AllowedMethods []string
	logger         *log.Logger
	database       *db.Database
}

func NewProxy(domain string, logger *log.Logger, database *db.Database) (*Proxy, error) {
	parsedDomain, err := url.Parse(domain)
	if err != nil {
		return nil, err
	}

	return &Proxy{
		AllowedMethods: []string{"GET", "OPTIONS", "HEAD"},
		Domain:         parsedDomain,
		logger:         logger,
		database:       database,
	}, nil
}

func (p *Proxy) ProxyFromRequest(req *http.Request) (*http.Response, error) {
	u := *req.URL
	u.Scheme = p.Domain.Scheme
	u.Host = p.Domain.Host
	u.RawPath = p.Domain.RawPath + u.RawPath

	p.logger.Info("Proxying route %s", u.String())
	nreq, err := http.NewRequest(req.Method, u.String(), req.Body)
	if err != nil {
		return nil, err
	}

	for k, vals := range req.Header {
		for _, v := range vals {
			nreq.Header.Add(k, v)
		}
	}

	for k, v := range p.Headers {
		nreq.Header.Add(k, v)
	}

	cli := &http.Client{
		Timeout: 20 * time.Second,
	}

	return cli.Do(nreq)
}

func (p *Proxy) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	headers, body, status, err := p.database.GetPageCache(req.Method, req.URL)
	if err != nil {
		if err == db.NoCacheErr {
			pres, err := p.ProxyFromRequest(req)
			if err != nil {
				p.logger.Error("Error on '%s' proxy: %s", req.URL, err.Error())
				api.WriteResponse(res, 500, api.NewErrorResponse(api.FromError(err)))
				return
			}

			pres.Header.Del("Transfer-Encoding")
			pres.Header.Del("Content-Length")

			headers = pres.Header
			status = pres.StatusCode
			body, err = ioutil.ReadAll(pres.Body)
			if err != nil {
				p.logger.Error("Error on proxy read '%s' proxy: %s", req.URL, err.Error())
				api.WriteResponse(res, 500, api.NewErrorResponse(api.FromError(err)))
				return
			}

			err = pres.Body.Close()
			if err != nil {
				p.logger.Error("Error on proxy read '%s' proxy: %s", req.URL, err.Error())
				api.WriteResponse(res, 500, api.NewErrorResponse(api.FromError(err)))
				return
			}

			if utils.InArray(p.AllowedMethods, req.Method) {
				p.logger.Info("Caching %s from %s", units.BytesSize(float64(len(body))), req.URL)
				err = p.database.CreatePageCache(req.URL, pres.Header, body, req.Method, pres.StatusCode)
				if err != nil {
					p.logger.Warn("Error on proxy save cache '%s' proxy: %s", req.URL, err.Error())
				}
			}
		} else {
			p.logger.Error("Error on '%s' proxy: %s", req.URL, err.Error())
			api.WriteResponse(res, 500, api.NewErrorResponse(api.FromError(err)))
			return
		}
	}

	for k, vals := range headers {
		for _, v := range vals {
			p.logger.Debug("Setting header '%s' -> '%s'", k, v)
			res.Header().Add(k, v)
		}
	}
	res.WriteHeader(status)
	_, _ = res.Write(body)
}
