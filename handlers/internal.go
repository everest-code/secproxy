package handlers

import (
	"encoding/base64"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/everest-code/golib/api"
	"gitlab.com/everest-code/golib/log"
	"gitlab.com/everest-code/secproxy/db"
	"gitlab.com/everest-code/secproxy/utils"
	"net/http"
	"regexp"
	"strings"
)

type InternalAPI struct {
	ApiKey   string
	logger   *log.Logger
	database *db.Database
}

const (
	InternalApiHeader  = "X-SecProxy-Api"
	InternalAuthHeader = "X-SecProxy-Authorization"
	DomainFrom         = "X-Api-From"
)

func NewInternalApi(logger *log.Logger, database *db.Database, apiKey *string) *InternalAPI {
	if apiKey == nil {
		pwd := "adminUser"
		hashed, _ := utils.GeneratePasswd(pwd)
		apiKey = &hashed
		logger.Warn("Api key was not set, default api key is: '%s'", pwd)
	}
	return &InternalAPI{
		ApiKey:   *apiKey,
		logger:   logger,
		database: database,
	}
}

func (handler *InternalAPI) CreateRouter(prefix string) *mux.Router {
	router := mux.NewRouter()
	api.SerializeRoutes(router, &api.RouteTree{
		Path: prefix,
		Handler: map[string]http.HandlerFunc{
			http.MethodGet: handler.internalLoginMiddleware(handler.Ping),
		},
		SubPaths: []api.RouteTree{
			{
				Path: "/internal",
				SubPaths: []api.RouteTree{
					{
						Path: "/cache",
						SubPaths: []api.RouteTree{
							{
								Path: "/status",
								Handler: map[string]http.HandlerFunc{
									http.MethodGet: handler.internalLoginMiddleware(handler.NewApiKey),
								},
							},
							{
								Path: "/clear",
								Handler: map[string]http.HandlerFunc{
									http.MethodDelete: handler.internalLoginMiddleware(handler.NewApiKey),
								},
							},
						},
					},
				},
			},
			{
				Path: "/token",
				SubPaths: []api.RouteTree{
					{
						Path: "/new",
						Handler: map[string]http.HandlerFunc{
							http.MethodPost: handler.internalLoginMiddleware(handler.NewApiKey),
						},
					},
					{
						Path: "/ping",
						Handler: map[string]http.HandlerFunc{
							http.MethodGet: handler.internalLoginMiddleware(handler.authTokenMiddleware(handler.PingApiKey, "Authorization")),
						},
					},
					{
						Path: "/logs",
						Handler: map[string]http.HandlerFunc{
							http.MethodGet: handler.internalLoginMiddleware(handler.authTokenMiddleware(handler.ApiKeyGetLogs, "Authorization")),
						},
					},
				},
			},
		},
	})
	return router
}

func (handler *InternalAPI) internalLoginMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		auth := req.Header.Get(InternalApiHeader)
		if auth == "" {
			api.WriteResponse(res, 401, api.NewErrorResponse(&api.Error{
				Name:        "HeaderError",
				Description: fmt.Sprintf("provide an '%s' header for InternalApi authentication", InternalApiHeader),
			}))
			return
		}

		authPasswd, err := base64.StdEncoding.DecodeString(auth)
		if err != nil {
			api.WriteResponse(res, 500, api.NewErrorResponse(api.FromError(err)))
			return
		}

		ok, err := utils.VerifyPassword(handler.ApiKey, string(authPasswd))
		if err != nil {
			api.WriteResponse(res, 500, api.NewErrorResponse(api.FromError(err)))
			return
		}

		if !ok {
			api.WriteResponse(res, 401, api.NewErrorResponse(&api.Error{
				Name: "BadCredentials",
			}))
			return
		}

		next(res, req)
	}
}

func (handler *InternalAPI) authTokenMiddleware(next http.HandlerFunc, header string) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		auth := req.Header.Get(header)
		if auth == "" {
			handler.logger.Warn("header '%s' not set", header)
			api.WriteResponse(res, 401, api.NewErrorResponse(&api.Error{
				Name: "BadCredentials",
			}))
			return
		}

		if !strings.HasPrefix(auth, "Basic") {
			handler.logger.Warn("header '%s' is not basic", header)
			api.WriteResponse(res, 401, api.NewErrorResponse(&api.Error{
				Name:        "BadCredentials",
				Description: "must be a 'Basic' authorization",
			}))
			return
		}

		re := regexp.MustCompile("^Basic (.+)$")
		match := re.FindSubmatch([]byte(auth))
		if len(match) != 2 {
			api.WriteResponse(res, 401, api.NewErrorResponse(&api.Error{
				Name: "BadCredentials",
			}))
			return
		}

		rawAuth, err := base64.StdEncoding.DecodeString(string(match[1]))
		if err != nil {
			handler.logger.Warn("Error decoding")
			api.WriteResponse(res, 500, api.NewErrorResponse(api.FromError(err)))
			return
		}

		authSplit := strings.SplitN(string(rawAuth), ":", 2)
		if len(authSplit) != 2 {
			handler.logger.Warn("header '%s' malformed", header)
			api.WriteResponse(res, 401, api.NewErrorResponse(&api.Error{
				Name: "BadCredentials",
			}))
			return
		}

		err = handler.database.CheckApiKey(authSplit[0], authSplit[1])
		if err != nil {
			if err == db.BadCredentials {
				handler.logger.Warn("Bad credentials")
				api.WriteResponse(res, 401, api.NewErrorResponse(&api.Error{
					Name: "BadCredentials",
				}))
				return
			}

			api.WriteResponse(res, 500, api.NewErrorResponse(api.FromError(err)))
			return
		}

		req.Header.Add(DomainFrom, authSplit[0])

		next(res, req)
	}
}

func (handler *InternalAPI) GlobalAuthMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return handler.authTokenMiddleware(func(res http.ResponseWriter, req *http.Request) {
		next(res, req)
		handler.database.AddRequest(req.Header.Get(DomainFrom), req.URL)
	}, InternalAuthHeader)
}

func (handler *InternalAPI) Ping(res http.ResponseWriter, req *http.Request) {
	api.WriteResponse(res, 202, api.NewOkResponse("OK"))
}

func (handler *InternalAPI) CacheStatus(res http.ResponseWriter, req *http.Request) {
	api.WriteResponse(res, 202, api.NewOkResponse(nil))
}

func (handler *InternalAPI) CacheClear(res http.ResponseWriter, req *http.Request) {
	api.WriteResponse(res, 202, api.NewOkResponse(nil))
}

func (handler *InternalAPI) NewApiKey(res http.ResponseWriter, req *http.Request) {
	val := make(map[string]string)
	if err := api.BindRequest(req, &val); err != nil {
		api.WriteResponse(res, 400, api.NewErrorResponse(api.FromError(err)))
		return
	}

	if val["domain"] == "" {
		api.WriteResponse(res, 400, api.NewErrorResponse(&api.Error{
			Name:        "ValidationError",
			Description: "domain value can not be empty",
		}))
		return
	}

	if ok, err := handler.database.KeyExists(val["domain"]); err != nil {
		api.WriteResponse(res, 500, api.NewErrorResponse(api.FromError(err)))
		return
	} else if ok {
		api.WriteResponse(res, 403, api.NewErrorResponse(&api.Error{
			Name:        "ValidationError",
			Description: "domain already exists",
		}))
		return
	}

	key, err := handler.database.CreateApiKey(val["domain"])
	if err != nil {
		api.WriteResponse(res, 500, api.NewErrorResponse(api.FromError(err)))
		return
	}

	api.WriteResponse(res, 201, api.NewOkResponse(map[string]string{
		"user":   val["domain"],
		"passwd": key,
	}))
}

func (handler *InternalAPI) PingApiKey(res http.ResponseWriter, req *http.Request) {
	api.WriteResponse(res, 202, api.NewOkResponse(req.Header.Get(DomainFrom)))
}

func (handler *InternalAPI) ApiKeyGetLogs(res http.ResponseWriter, req *http.Request) {
	resData, err := handler.database.ComposeKeyLog(req.Header.Get(DomainFrom))
	if err != nil {
		api.WriteResponse(res, 500, api.NewErrorResponse(api.FromError(err)))
		return
	}

	api.WriteResponse(res, 200, api.NewOkResponse(resData))
}
