package main

import (
	"github.com/go-redis/redis/v8"
	"github.com/pelletier/go-toml/v2"
	"gitlab.com/everest-code/golib/log"
	"io/ioutil"
	"os"
)

type Config struct {
	Port    string            `toml:"Expose"`
	Headers map[string]string `toml:"Headers"`
	DB      *redis.Options    `toml:"Database"`
	Api     struct {
		Key    *string `toml:"Key"`
		Prefix string  `toml:"Prefix"`
	} `toml:"Api"`
	Proxy struct {
		Domain         string   `toml:"Domain"`
		CacheTTL       int      `toml:"TTL"`
		AllowedMethods []string `toml:"AllowedMethods"`
	} `toml:"Proxy"`
}

func loadConf() *Config {
	name := os.Getenv("CONF_FILE")
	if name == "" {
		name = "./config.toml"
	}

	file, err := os.Open(name)
	if err != nil {
		log.Panic(err.Error())
	}

	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Panic(err.Error())
		}
	}(file)

	body, err := ioutil.ReadAll(file)
	if err != nil {
		log.Panic(err.Error())
	}

	conf := new(Config)
	err = toml.Unmarshal(body, conf)
	if err != nil {
		log.Panic(err.Error())
	}

	return conf
}
