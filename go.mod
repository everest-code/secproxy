module gitlab.com/everest-code/secproxy

go 1.17

require (
	github.com/docker/go-units v0.4.0
	github.com/go-redis/redis/v8 v8.11.5
	github.com/gorilla/mux v1.8.0
	github.com/pelletier/go-toml/v2 v2.0.2
	github.com/tredoe/osutil/v2 v2.0.0-rc.16
	gitlab.com/everest-code/golib/api v1.1.1
	gitlab.com/everest-code/golib/log v1.0.2
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
)
