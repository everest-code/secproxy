package main

import (
	"github.com/docker/go-units"
	"gitlab.com/everest-code/golib/log"
	"net/http"
	"time"
)

var (
	logger *log.Logger
	server *http.Server
)

func createServer(config *Config) *http.Server {
	server := newServer(config)

	return &http.Server{
		Addr:              config.Port,
		Handler:           server,
		ReadTimeout:       5 * time.Minute,
		ReadHeaderTimeout: 10 * time.Second,
		WriteTimeout:      5 * time.Minute,
		IdleTimeout:       40 * time.Second,
		MaxHeaderBytes:    5 * units.MB,
	}
}

func main() {
	logger, _ = log.NewConsole(log.LevelDebg, false)
	conf := loadConf()
	server = createServer(conf)
	logger.Debug("Server created")
	logger.ImportantInfo("Server listening on '%s'", conf.Port)
	if err := server.ListenAndServe(); err != nil {
		logger.Error(err.Error())
	}
}
